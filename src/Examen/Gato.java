package Examen;

import static java.lang.Math.random;
import javax.swing.JOptionPane;

public class Gato extends javax.swing.JFrame {
    String var = "X";
   
    public Gato() {
        initComponents();
    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        B1 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();
        B3 = new javax.swing.JButton();
        B4 = new javax.swing.JButton();
        B5 = new javax.swing.JButton();
        B6 = new javax.swing.JButton();
        B7 = new javax.swing.JButton();
        B8 = new javax.swing.JButton();
        B9 = new javax.swing.JButton();
        Rest = new javax.swing.JButton();
        Trma = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        B1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });

        B2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2ActionPerformed(evt);
            }
        });

        B3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B3ActionPerformed(evt);
            }
        });

        B4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B4ActionPerformed(evt);
            }
        });

        B5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B5ActionPerformed(evt);
            }
        });

        B6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B6ActionPerformed(evt);
            }
        });

        B7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B7ActionPerformed(evt);
            }
        });

        B8.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B8ActionPerformed(evt);
            }
        });

        B9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        B9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B9ActionPerformed(evt);
            }
        });

        Rest.setText("Restart");
        Rest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RestActionPerformed(evt);
            }
        });

        Trma.setText("Tiro Maquina");
        Trma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TrmaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(B7, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                            .addComponent(B4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(B1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(B2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(B5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(B8, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(B3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(B6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(B9, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addComponent(Rest, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(74, 74, 74)
                        .addComponent(Trma, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Rest, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                    .addComponent(Trma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(B3, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(B2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(B1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(B4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(B5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(B6, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(B7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(B8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(B9, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RestActionPerformed
        B1.setText("");
        B2.setText("");
        B3.setText("");
        B4.setText("");
        B5.setText("");
        B6.setText("");
        B7.setText("");
        B8.setText("");
        B9.setText("");
    }//GEN-LAST:event_RestActionPerformed

    private void TrmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TrmaActionPerformed
        boolean resp = false;
        String uno = B1.getText();
        String dos = B2.getText();
        String tres = B3.getText();
        String cuatro = B4.getText();
        String cinco = B5.getText();
        String seis = B6.getText();
        String siete = B7.getText();
        String ocho = B8.getText();
        String nueve = B9.getText();
        do{
            int numerorandom = (int)(Math.random()*9+1);
            switch(numerorandom){
                   case 1:{ if(uno!="X"&&uno!="O"){B1.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 2:{ if(dos!="X"&&dos!="O"){B2.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 3:{ if(tres!="X"&&tres!="O"){B3.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 4:{ if(cuatro!="X"&&cuatro!="O"){B4.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 5:{ if(cinco!="X"&&cinco!="O"){B5.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 6:{ if(seis!="X"&&seis!="O"){B6.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 7:{ if(siete!="X"&&siete!="O"){B7.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 8:{ if(ocho!="X"&&ocho!="O"){B8.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
                   case 9:{ if(nueve!="X"&&nueve!="O"){B9.setText("O"); resp=true;} 
                        Ganador();
                   }
                   break;
               } 
        }while(resp!=true);                                           
    }//GEN-LAST:event_TrmaActionPerformed

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed
        B1.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B1ActionPerformed

    private void B2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2ActionPerformed
        B2.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B2ActionPerformed

    private void B3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B3ActionPerformed
        B3.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B3ActionPerformed

    private void B4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B4ActionPerformed
        B4.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B4ActionPerformed

    private void B5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B5ActionPerformed
        B5.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B5ActionPerformed

    private void B6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B6ActionPerformed
        B6.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B6ActionPerformed

    private void B7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B7ActionPerformed
        B7.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B7ActionPerformed

    private void B8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B8ActionPerformed
        B8.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B8ActionPerformed

    private void B9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B9ActionPerformed
        B9.setText(var);
        if(var.equals("X")){
           var = "O";
        }
        else{
           var = "X";
        }
        Ganador();
    }//GEN-LAST:event_B9ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Gato().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JButton B3;
    private javax.swing.JButton B4;
    private javax.swing.JButton B5;
    private javax.swing.JButton B6;
    private javax.swing.JButton B7;
    private javax.swing.JButton B8;
    private javax.swing.JButton B9;
    private javax.swing.JButton Rest;
    private javax.swing.JButton Trma;
    // End of variables declaration//GEN-END:variables

    private void Ganador() {
         String uno = B1.getText();
        String dos = B2.getText();
        String tres = B3.getText();
        String cuatro = B4.getText();
        String cinco = B5.getText();
        String seis = B6.getText();
        String siete = B7.getText();
        String ocho = B8.getText();
        String nueve = B9.getText();
        
        
        
        if(uno.equals("X")&& dos.equals("X")&& tres.equals("X")){
            GanoX();    
        } else if(uno.equals("O")&& dos.equals("O")&& tres.equals("O")){
            GanoO();
        } else if(cuatro.equals("X")&& cinco.equals("X")&& seis.equals("X")){
            GanoX();
        } else if(cuatro.equals("O")&& cinco.equals("O")&& seis.equals("O")){
            GanoO();
        } else if(siete.equals("X")&& ocho.equals("X")&& nueve.equals("X")){
            GanoX();
        } else if(siete.equals("O")&& ocho.equals("O")&& nueve.equals("O")){
            GanoO();
        } else if(uno.equals("X")&& cuatro.equals("X")&& siete.equals("X")){
            GanoX();
        } else if(uno.equals("O")&& cuatro.equals("O")&& siete.equals("O")){
            GanoO();
        } else if(dos.equals("X")&& cinco.equals("X")&& ocho.equals("X")){
            GanoX();
        } else if(dos.equals("O")&& cinco.equals("O")&& ocho.equals("O")){
            GanoO();
        } else if(tres.equals("X")&& seis.equals("X")&& nueve.equals("X")){
            GanoX();
        } else if(tres.equals("O")&& seis.equals("O")&& nueve.equals("O")){
            GanoO();
        } else if(uno.equals("X")&& cinco.equals("X")&& nueve.equals("X")){
            GanoX();
        } else if(uno.equals("O")&& cinco.equals("O")&& nueve.equals("O")){
            GanoO();
        } else if(tres.equals("X")&& cinco.equals("X")&& siete.equals("X")){
            GanoX();
        } else if(tres.equals("O")&& cinco.equals("O")&& siete.equals("O")){
            GanoO();
        } else{
            Empate();
        }
        
                                                                        
    }
    
    private void GanoX(){
        JOptionPane.showMessageDialog(null, "GANARON LAS X");
        B1.setText("");
        B2.setText("");
        B3.setText("");
        B4.setText("");
        B5.setText("");
        B6.setText("");
        B7.setText("");
        B8.setText("");
        B9.setText("");
    }
    
    private void GanoO(){
        JOptionPane.showMessageDialog(null, "GANARON LAS O");
        B1.setText("");
        B2.setText("");
        B3.setText("");
        B4.setText("");
        B5.setText("");
        B6.setText("");
        B7.setText("");
        B8.setText("");
        B9.setText("");
    }
    
    private void Empate(){
        
        String uno = B1.getText();
        String dos = B2.getText();
        String tres = B3.getText();
        String cuatro = B4.getText();
        String cinco = B5.getText();
        String seis = B6.getText();
        String siete = B7.getText();
        String ocho = B8.getText();
        String nueve = B9.getText();
        
        if(uno!=""&&dos!=""&&tres!=""&&cuatro!=""&&cinco!=""&&seis!=""&&siete!=""&&ocho!=""&&nueve!=""){
                JOptionPane.showMessageDialog(null, "EMPATE");
        }
        
    }
}

